const texto1 = 'Olá, mundo!';
const texto2 = 'Olá mundo!';
const senha = 'senha';
const stringdenumeros = '34567';

const citacao = 'Meu nome é ';
const meunome = 'Leonardo';

// concatenação (+)

console.log(citacao + meunome)

// template string ou template literal

var cifrao = '\u0024'
console.log(cifrao)