// boolean
const usuariologado = true
const contapaga = false

// truthy ou falsy

// 0 => false
// 1 => true

/*console.log(0 == false)
console.log('' == false)
console.log(1 == true)*/

// undefined

// null ==> vazio ou mais 

let minhavar;
let varNull = null;

let numero = 3;
let texto = 'Alura';

console.log(typeof minhavar)
console.log(typeof varNull)
