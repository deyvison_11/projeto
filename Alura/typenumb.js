// tipo Number

const meuNumero = 3;

const n1 = 1;
const n2 = 2;

const operacaoMatematica =  n1 - n2;

console.log(operacaoMatematica)

// ponto flutuante

const numeroPontoFlutuante = 3.3;
const pontoFlutuanteSemZero = .5;

const novaOperação = n1 / numeroPontoFlutuante;

console.log(novaOperação)

// NaN -> Not A Number (não é um número)

const alura = 'Alura';
console.log(alura * n1)