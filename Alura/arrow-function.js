function apresentar(nome) {
    return `meu nome é ${nome}`;
}

// Arrow function
const apresentararrow = nome => `meu nome é ${nome}`;
const soma = (num1, num2) => num1 + num2;

// Arrow function com + de 1 linha de introdução

const somanumerospequenos = (num1, num2) => {
    if (num1 || num2 > 10) {
        return 'Somente números de 1 a 9';
    } else {
        return num1 + num2;
    }
}

// Hosting: arrow function se comporta como expressão

// operador maior ou igual que: >=